export default {
  mode: 'spa',
  router: {
    base: '/front/'
  },
  head: {
    title: 'Ozone',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {
        hid: 'description',
        name: 'description',
        content: 'Мы любим своих клиентов, поэтому делаем все возможное, чтобы ваши занятия в клубе проходили комфортно и с удовольствием!Представляем вам обновлённый фитнес-клуб OZONE, к вашим услугам: тренажёрный зал, зона кроссфита, кардио зона, групповые программы, йога, персональные тренировки, EMS-тренировки, солярий, финская сауна'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'Фитнес, спорт, зож, правильное питание, пп, кроссфит, йога, пилатес, тренажёры, силовые тренировки, растяжка, велотренажеры, эллипсы, сауна, солярий, кардио, персональные тренировки, Здоровье, Латина, качалка, групповые занятия, мягкий фитнес, емс'
      }

    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', href: '/fonts/stylesheet.css'},
      {rel: 'stylesheet', href: '/style.css'},
      {rel: 'stylesheet', href: 'https://clients.streamwood.ru/StreamWood/sw.css', type: 'text/css'}
    ],
    script: [
      {src: 'https://clients.streamwood.ru/StreamWood/sw.js'}
    ],
  },

  loading: {color: '#fff'},

  css: [],

  plugins: [
    '~/static/StreamWood'
  ],

  buildModules: [],

  modules: [],

  build: {

    extend(config, ctx) {
    }
  }
}
