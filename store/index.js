export const state = () => ({
  burger : false
});

export const mutations = {
    setBurger(state){
        state.burger = !state.burger
    }
};

export const actions =  {
  setBurger({commit}){
    commit('setBurger')
  }
};

export const getters = {
  getBurger (state) {
    return state.burger;
  }
};
